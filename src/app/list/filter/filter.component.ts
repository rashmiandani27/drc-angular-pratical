import { LabelType, Options } from '@angular-slider/ngx-slider';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ListModel } from '../list.component';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit {
  minValue: number = 100;
  maxValue: number = 400;
  options: Options = {
    floor: 0,
    ceil: 500,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return '<b>Min price:</b> $' + value;
        case LabelType.High:
          return '<b>Max price:</b> $' + value;
        default:
          return '$' + value;
      }
    },
  };

  loc: string = '';

  @Input() data: ListModel[] = [];
  @Output() onFilter = new EventEmitter<ListModel[]>();
  filterdata: ListModel[] = [];
  checkedList: string[] = [];
  constructor() {}

  ngOnInit(): void {}

  onSliderClick() {
    // this.data.filter(ele => this.value<ele.);
  }
  checkFilter(id: string) {
    this.filterdata = [];
    this.checkedList.push(id);
    this.checkedList.forEach((element) => {
      let fildata: ListModel[] = this.data.filter(
        (ele) => ele.RAM.split('DD')[0] === element
      );
      this.filterdata = [...this.filterdata, ...fildata];
    });

    this.onFilter.emit(this.filterdata);
  }

  hddFilter(event: any) {
    let selected = event?.target?.value;
    let fildata: ListModel[] = this.data.filter((ele) =>
      ele.HDD.includes(selected)
    );
    this.filterdata = [...fildata];
    this.onFilter.emit(this.filterdata);
  }
  locFilter(event: any) {
    let selected = event?.target?.value;
    let fildata: ListModel[] = this.data.filter((ele) =>
      ele.Location.includes(selected)
    );
    this.filterdata = [...fildata];
    this.onFilter.emit(this.filterdata);
  }
}
